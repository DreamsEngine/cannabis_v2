<?php
// Google
// GTM
require_once INC_DIR . '/plugins/google/gtm.php';

// MINIFY STYLES
if (!function_exists('minify')):
	function minify($who)
	{
		$buffer = '';

		$compress = $who;

		$buffer = $compress;
		// Remove comments
		$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
		// Remove space after colons
		$buffer = str_replace(': ', ':', $buffer);
		// Remove whitespace
		$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);

		return $buffer;
	}
endif;

// Bring CSS INLINE
if (!function_exists('mvp_styles_method')) {
	function mvp_styles_method()
	{
		wp_enqueue_style('mvp-custom-style', get_stylesheet_uri());
		$wallad = get_option('mvp_wall_ad');
		$primarycolor = get_option('mvp_primary_color');
		$secondcolor = get_option('mvp_second_color');
		$topnavbg = get_option('mvp_top_nav_bg');
		$topnavtext = get_option('mvp_top_nav_text');
		$topnavhover = get_option('mvp_top_nav_hover');
		$botnavbg = get_option('mvp_bot_nav_bg');
		$botnavtext = get_option('mvp_bot_nav_text');
		$botnavhover = get_option('mvp_bot_nav_hover');
		$link = get_option('mvp_link_color');
		$link2 = get_option('mvp_link2_color');
		$featured_font = get_option('mvp_featured_font');
		$title_font = get_option('mvp_title_font');
		$heading_font = get_option('mvp_heading_font');
		$content_font = get_option('mvp_content_font');
		$para_font = get_option('mvp_para_font');
		$menu_font = get_option('mvp_menu_font');
		$mvp_customcss = get_option('mvp_customcss');
		$mvp_theme_options = "
    #idliga{
        background-color: #000; 
        color: #fff; 
        width: 100%; 
        font-size: 1rem;  
    }

    .widget_score{
        display: flex; 
        flex-direction: row; 
        background-color: #1F1F1F;
    }

    .widget_score_select{
        width: 10%; 
        display: flex; 
        justify-content: center; 
        align-items: center; 
        padding: 0 .75em;
    }
    .widget_score_match{
        width: 90%;
        height: 100px;
    }

    @media screen and (max-width: 768px){
        .widget_score{
            flex-direction: column; 
        }
        .widget_score_select{
            width: 90%; 
            padding: .75em;
            margin: 0 auto;
        }
        .widget_score_match{
            width: 100%;
        }
    }

    #mvp-wallpaper {
        background: url($wallad) no-repeat 50% 0;
        }
    
    #mvp-foot-copy a {
        color: $link;
        }
    
    #mvp-content-main p a,
    .mvp-post-add-main p a {
        box-shadow: inset 0 -4px 0 $link;
        }
    
    #mvp-content-main p a:hover,
    .mvp-post-add-main p a:hover {
        background: $link;
        }
    
    a,
    a:visited,
    .post-info-name a,
    .woocommerce .woocommerce-breadcrumb a {
        color: $link2;
        }
    
    #mvp-side-wrap a:hover {
        color: $link2;
        }
    
    .mvp-fly-top:hover,
    .mvp-vid-box-wrap,
    ul.mvp-soc-mob-list li.mvp-soc-mob-com {
        background: $primarycolor;
        }
    
    nav.mvp-fly-nav-menu ul li.menu-item-has-children:after,
    .mvp-feat1-left-wrap span.mvp-cd-cat,
    .mvp-widget-feat1-top-story span.mvp-cd-cat,
    .mvp-widget-feat2-left-cont span.mvp-cd-cat,
    .mvp-widget-dark-feat span.mvp-cd-cat,
    .mvp-widget-dark-sub span.mvp-cd-cat,
    .mvp-vid-wide-text span.mvp-cd-cat,
    .mvp-feat2-top-text span.mvp-cd-cat,
    .mvp-feat3-main-story span.mvp-cd-cat,
    .mvp-feat3-sub-text span.mvp-cd-cat,
    .mvp-feat4-main-text span.mvp-cd-cat,
    .woocommerce-message:before,
    .woocommerce-info:before,
    .woocommerce-message:before {
        color: $primarycolor;
        }
    
    #searchform input,
    .mvp-authors-name {
        border-bottom: 1px solid $primarycolor;
        }
    
    .mvp-fly-top:hover {
        border-top: 1px solid $primarycolor;
        border-left: 1px solid $primarycolor;
        border-bottom: 1px solid $primarycolor;
        }
    
    .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
    .woocommerce #respond input#submit.alt,
    .woocommerce a.button.alt,
    .woocommerce button.button.alt,
    .woocommerce input.button.alt,
    .woocommerce #respond input#submit.alt:hover,
    .woocommerce a.button.alt:hover,
    .woocommerce button.button.alt:hover,
    .woocommerce input.button.alt:hover {
        background-color: $primarycolor;
        }
    
    .woocommerce-error,
    .woocommerce-info,
    .woocommerce-message {
        border-top-color: $primarycolor;
        }
    
    ul.mvp-feat1-list-buts li.active span.mvp-feat1-list-but,
    span.mvp-widget-home-title,
    span.mvp-post-cat,
    span.mvp-feat1-pop-head {
        background: $secondcolor;
        }
    
    .woocommerce span.onsale {
        background-color: $secondcolor;
        }
    
    .mvp-widget-feat2-side-more-but,
    .woocommerce .star-rating span:before,
    span.mvp-prev-next-label,
    .mvp-cat-date-wrap .sticky {
        color: $secondcolor !important;
        }
    
    #mvp-main-nav-top,
    #mvp-fly-wrap,
    .mvp-soc-mob-right,
    #mvp-main-nav-small-cont {
        background: $topnavbg;
        }
    
    #mvp-main-nav-small .mvp-fly-but-wrap span,
    #mvp-main-nav-small .mvp-search-but-wrap span,
    .mvp-nav-top-left .mvp-fly-but-wrap span,
    #mvp-fly-wrap .mvp-fly-but-wrap span {
        background: $topnavtext;
        }
    
    .mvp-nav-top-right .mvp-nav-search-but,
    span.mvp-fly-soc-head,
    .mvp-soc-mob-right i,
    #mvp-main-nav-small span.mvp-nav-search-but,
    #mvp-main-nav-small .mvp-nav-menu ul li a  {
        color: $topnavtext;
        }
    
    #mvp-main-nav-small .mvp-nav-menu ul li.menu-item-has-children a:after {
        border-color: $topnavtext transparent transparent transparent;
        }
    
    #mvp-nav-top-wrap span.mvp-nav-search-but:hover,
    #mvp-main-nav-small span.mvp-nav-search-but:hover {
        color: $topnavhover;
        }
    
    #mvp-nav-top-wrap .mvp-fly-but-wrap:hover span,
    #mvp-main-nav-small .mvp-fly-but-wrap:hover span,
    span.mvp-woo-cart-num:hover {
        background: $topnavhover;
        }
    
    #mvp-main-nav-bot-cont {
        background: $botnavbg;
        }
    
    #mvp-nav-bot-wrap .mvp-fly-but-wrap span,
    #mvp-nav-bot-wrap .mvp-search-but-wrap span {
        background: $botnavtext;
        }
    
    #mvp-nav-bot-wrap span.mvp-nav-search-but,
    #mvp-nav-bot-wrap .mvp-nav-menu ul li a {
        color: $botnavtext;
        }
    
    #mvp-nav-bot-wrap .mvp-nav-menu ul li.menu-item-has-children a:after {
        border-color: $botnavtext transparent transparent transparent;
        }
    
    .mvp-nav-menu ul li:hover a {
        border-bottom: 5px solid $botnavhover;
        }
    
    #mvp-nav-bot-wrap .mvp-fly-but-wrap:hover span {
        background: $botnavhover;
        }
    
    #mvp-nav-bot-wrap span.mvp-nav-search-but:hover {
        color: $botnavhover;
        }
    
    body,
    .mvp-feat1-feat-text p,
    .mvp-feat2-top-text p,
    .mvp-feat3-main-text p,
    .mvp-feat3-sub-text p,
    #searchform input,
    .mvp-author-info-text,
    span.mvp-post-excerpt,
    .mvp-nav-menu ul li ul.sub-menu li a,
    nav.mvp-fly-nav-menu ul li a,
    .mvp-ad-label,
    span.mvp-feat-caption,
    .mvp-post-tags a,
    .mvp-post-tags a:visited,
    span.mvp-author-box-name a,
    #mvp-author-box-text p,
    .mvp-post-gallery-text p,
    ul.mvp-soc-mob-list li span,
    #comments,
    h3#reply-title,
    h2.comments,
    #mvp-foot-copy p,
    span.mvp-fly-soc-head,
    .mvp-post-tags-header,
    span.mvp-prev-next-label,
    span.mvp-post-add-link-but,
    #mvp-comments-button a,
    #mvp-comments-button span.mvp-comment-but-text,
    .woocommerce ul.product_list_widget span.product-title,
    .woocommerce ul.product_list_widget li a,
    .woocommerce #reviews #comments ol.commentlist li .comment-text p.meta,
    .woocommerce div.product p.price,
    .woocommerce div.product p.price ins,
    .woocommerce div.product p.price del,
    .woocommerce ul.products li.product .price del,
    .woocommerce ul.products li.product .price ins,
    .woocommerce ul.products li.product .price,
    .woocommerce #respond input#submit,
    .woocommerce a.button,
    .woocommerce button.button,
    .woocommerce input.button,
    .woocommerce .widget_price_filter .price_slider_amount .button,
    .woocommerce span.onsale,
    .woocommerce-review-link,
    #woo-content p.woocommerce-result-count,
    .woocommerce div.product .woocommerce-tabs ul.tabs li a,
    a.mvp-inf-more-but,
    span.mvp-cont-read-but,
    span.mvp-cd-cat,
    span.mvp-cd-date,
    .mvp-feat4-main-text p,
    span.mvp-woo-cart-num,
    span.mvp-widget-home-title2,
    .wp-caption,
    #mvp-content-main p.wp-caption-text,
    .gallery-caption,
    .mvp-post-add-main p.wp-caption-text,
    #bbpress-forums,
    #bbpress-forums p,
    .protected-post-form input,
    #mvp-feat6-text p {
        font-family: '$content_font', sans-serif;
        }
    
    .mvp-blog-story-text p,
    span.mvp-author-page-desc,
    #mvp-404 p,
    .mvp-widget-feat1-bot-text p,
    .mvp-widget-feat2-left-text p,
    .mvp-flex-story-text p,
    .mvp-search-text p,
    #mvp-content-main p,
    .mvp-post-add-main p,
    #mvp-content-main ul li,
    #mvp-content-main ol li,
    .rwp-summary,
    .rwp-u-review__comment,
    .mvp-feat5-mid-main-text p,
    .mvp-feat5-small-main-text p,
    #mvp-content-main .wp-block-button__link,
    .wp-block-audio figcaption,
    .wp-block-video figcaption,
    .wp-block-embed figcaption,
    .wp-block-verse pre,
    pre.wp-block-verse {
        font-family: '$para_font', sans-serif;
        }
    
    .mvp-nav-menu ul li a,
    #mvp-foot-menu ul li a {
        font-family: '$menu_font', sans-serif;
        }
    
    
    .mvp-feat1-sub-text h2,
    .mvp-feat1-pop-text h2,
    .mvp-feat1-list-text h2,
    .mvp-widget-feat1-top-text h2,
    .mvp-widget-feat1-bot-text h2,
    .mvp-widget-dark-feat-text h2,
    .mvp-widget-dark-sub-text h2,
    .mvp-widget-feat2-left-text h2,
    .mvp-widget-feat2-right-text h2,
    .mvp-blog-story-text h2,
    .mvp-flex-story-text h2,
    .mvp-vid-wide-more-text p,
    .mvp-prev-next-text p,
    .mvp-related-text,
    .mvp-post-more-text p,
    h2.mvp-authors-latest a,
    .mvp-feat2-bot-text h2,
    .mvp-feat3-sub-text h2,
    .mvp-feat3-main-text h2,
    .mvp-feat4-main-text h2,
    .mvp-feat5-text h2,
    .mvp-feat5-mid-main-text h2,
    .mvp-feat5-small-main-text h2,
    .mvp-feat5-mid-sub-text h2,
    #mvp-feat6-text h2,
    .alp-related-posts-wrapper .alp-related-post .post-title {
        font-family: '$featured_font', sans-serif;
        }
    
    .mvp-feat2-top-text h2,
    .mvp-feat1-feat-text h2,
    h1.mvp-post-title,
    h1.mvp-post-title-wide,
    .mvp-drop-nav-title h4,
    #mvp-content-main blockquote p,
    .mvp-post-add-main blockquote p,
    #mvp-content-main p.has-large-font-size,
    #mvp-404 h1,
    #woo-content h1.page-title,
    .woocommerce div.product .product_title,
    .woocommerce ul.products li.product h3,
    .alp-related-posts .current .post-title {
        font-family: '$title_font', sans-serif;
        }
    
    span.mvp-feat1-pop-head,
    .mvp-feat1-pop-text:before,
    span.mvp-feat1-list-but,
    span.mvp-widget-home-title,
    .mvp-widget-feat2-side-more,
    span.mvp-post-cat,
    span.mvp-page-head,
    h1.mvp-author-top-head,
    .mvp-authors-name,
    #mvp-content-main h1,
    #mvp-content-main h2,
    #mvp-content-main h3,
    #mvp-content-main h4,
    #mvp-content-main h5,
    #mvp-content-main h6,
    .woocommerce .related h2,
    .woocommerce div.product .woocommerce-tabs .panel h2,
    .woocommerce div.product .product_title,
    .mvp-feat5-side-list .mvp-feat1-list-img:after {
        font-family: '$heading_font', sans-serif;
        }
    
        ";

		if (get_option('mvp_wall_ad')) {
			$mvp_wall_ad_css = "
        @media screen and (min-width: 1200px) {
        #mvp-site {
            float: none;
            margin: 0 auto;
            width: 1200px;
            }
        #mvp-leader-wrap {
            left: auto;
            width: 1200px;
            }
        .mvp-main-box {
            width: 1160px;
            }
        #mvp-main-nav-top,
        #mvp-main-nav-bot,
        #mvp-main-nav-small {
            width: 1200px;
            }
        }
            ";
		}

		$mvp_site_skin = get_option('mvp_site_skin');
		if ($mvp_site_skin == '1') {
			$mvp_site_skin_css = "
        #mvp-main-nav-top {
            background: #fff;
            padding: 15px 0 0;
            }
        #mvp-fly-wrap,
        .mvp-soc-mob-right,
        #mvp-main-nav-small-cont {
            background: #fff;
            }
        #mvp-main-nav-small .mvp-fly-but-wrap span,
        #mvp-main-nav-small .mvp-search-but-wrap span,
        .mvp-nav-top-left .mvp-fly-but-wrap span,
        #mvp-fly-wrap .mvp-fly-but-wrap span {
            background: #000;
            }
        .mvp-nav-top-right .mvp-nav-search-but,
        span.mvp-fly-soc-head,
        .mvp-soc-mob-right i,
        #mvp-main-nav-small span.mvp-nav-search-but,
        #mvp-main-nav-small .mvp-nav-menu ul li a  {
            color: #000;
            }
        #mvp-main-nav-small .mvp-nav-menu ul li.menu-item-has-children a:after {
            border-color: #000 transparent transparent transparent;
            }
        .mvp-feat1-feat-text h2,
        h1.mvp-post-title,
        .mvp-feat2-top-text h2,
        .mvp-feat3-main-text h2,
        #mvp-content-main blockquote p,
        .mvp-post-add-main blockquote p {
            font-family: 'Anton', sans-serif;
            font-weight: 400;
            letter-spacing: normal;
            }
        .mvp-feat1-feat-text h2,
        .mvp-feat2-top-text h2,
        .mvp-feat3-main-text h2 {
            line-height: 1;
            text-transform: uppercase;
            }
            ";
		}

		$mvp_nav_skin = get_option('mvp_nav_skin');
		$mvp_site_skin = get_option('mvp_site_skin');
		if ($mvp_nav_skin == '1' || $mvp_site_skin == '1') {
			$mvp_nav_skin_css = "
        span.mvp-nav-soc-but,
        ul.mvp-fly-soc-list li a,
        span.mvp-woo-cart-num {
            background: rgba(0,0,0,.8);
            }
        span.mvp-woo-cart-icon {
            color: rgba(0,0,0,.8);
            }
        nav.mvp-fly-nav-menu ul li,
        nav.mvp-fly-nav-menu ul li ul.sub-menu {
            border-top: 1px solid rgba(0,0,0,.1);
            }
        nav.mvp-fly-nav-menu ul li a {
            color: #000;
            }
        .mvp-drop-nav-title h4 {
            color: #000;
            }
            ";
		}

		$mvp_nav_layout = get_option('mvp_nav_layout');
		if ($mvp_nav_layout == '1') {
			$mvp_nav_layout_css = "
        #mvp-main-body-wrap {
            padding-top: 20px;
            }
        #mvp-feat2-wrap,
        #mvp-feat4-wrap,
        #mvp-post-feat-img-wide,
        #mvp-vid-wide-wrap {
            margin-top: -20px;
            }
        @media screen and (max-width: 479px) {
            #mvp-main-body-wrap {
                padding-top: 15px;
                }
            #mvp-feat2-wrap,
            #mvp-feat4-wrap,
            #mvp-post-feat-img-wide,
            #mvp-vid-wide-wrap {
                margin-top: -15px;
                }
            }
            ";
		}

		$mvp_prime_skin = get_option('mvp_prime_skin');
		if ($mvp_prime_skin == '1') {
			$mvp_prime_skin_css = "
        .mvp-vid-box-wrap,
        .mvp-feat1-left-wrap span.mvp-cd-cat,
        .mvp-widget-feat1-top-story span.mvp-cd-cat,
        .mvp-widget-feat2-left-cont span.mvp-cd-cat,
        .mvp-widget-dark-feat span.mvp-cd-cat,
        .mvp-widget-dark-sub span.mvp-cd-cat,
        .mvp-vid-wide-text span.mvp-cd-cat,
        .mvp-feat2-top-text span.mvp-cd-cat,
        .mvp-feat3-main-story span.mvp-cd-cat {
            color: #fff;
            }
            ";
		}

		$mvp_para_lead = get_option('mvp_para_lead');
		if ($mvp_para_lead == 'true') {
			if (isset($mvp_para_lead)) {
			}
		} else {
			$mvp_para_lead_css = "
        #mvp-leader-wrap {
            position: relative;
            }
        #mvp-site-main {
            margin-top: 0;
            }
        #mvp-leader-wrap {
            top: 0 !important;
            }
            ";
		}

		$mvp_infinite_scroll = get_option('mvp_infinite_scroll');
		if ($mvp_infinite_scroll == 'true') {
			if (isset($mvp_infinite_scroll)) {
				$mvp_infinite_scroll_css = "
        .mvp-nav-links {
            display: none;
            }
            ";
			}
		}

		$mvp_respond = get_option('mvp_respond');
		if ($mvp_respond == 'true') {
			if (isset($mvp_respond)) {
			}
		} else {
			$mvp_respond_css = "
        #mvp-site,
        #mvp-main-nav-top,
        #mvp-main-nav-bot {
            min-width: 1240px;
            }
            ";
		}

		$mvp_alp = get_option('mvp_alp');
		if ($mvp_alp !== 'true') {
			$mvp_cont_read = get_option('mvp_cont_read');
			if ($mvp_cont_read == 'true') {
				if (isset($mvp_cont_read)) {
					$mvp_cont_read_css = "
        @media screen and (max-width: 479px) {
            .single #mvp-content-body-top {
                max-height: 400px;
                }
            .single .mvp-cont-read-but-wrap {
                display: inline;
                }
            }
            ";
				}
			}
		}

		$mvp_rtl = get_option('mvp_rtl');
		if ($mvp_rtl == 'true') {
			global $post;
			if (!empty($post)) {
				$mvp_post_layout = get_option('mvp_post_layout');
				$mvp_post_temp = get_post_meta($post->ID, 'mvp_post_template', true);
				if (
					(empty($mvp_post_temp) && $mvp_post_layout == '1') ||
					($mvp_post_temp == 'def' && $mvp_post_layout == '1') ||
					(empty($mvp_post_temp) && $mvp_post_layout == '7') ||
					($mvp_post_temp == 'def' && $mvp_post_layout == '7') ||
					($mvp_post_temp == 'global' && $mvp_post_layout == '1') ||
					($mvp_post_temp == 'global' && $mvp_post_layout == '7') ||
					$mvp_post_temp == 'temp2' ||
					$mvp_post_temp == 'temp8'
				) {
					$mvp_post_side_css = "
        .mvp-post-main-out,
        .mvp-post-main-in {
            margin-left: 0 !important;
            }
        #mvp-post-feat-img img {
            width: 100%;
            }
        #mvp-content-wrap,
        #mvp-post-add-box {
            float: none;
            margin: 0 auto;
            max-width: 750px;
            }
            ";
				}
			}
		} else {
			global $post;
			if (!empty($post)) {
				$mvp_post_layout = get_option('mvp_post_layout');
				$mvp_post_temp = get_post_meta($post->ID, 'mvp_post_template', true);
				if (
					(empty($mvp_post_temp) && $mvp_post_layout == '1') ||
					($mvp_post_temp == 'def' && $mvp_post_layout == '1') ||
					(empty($mvp_post_temp) && $mvp_post_layout == '7') ||
					($mvp_post_temp == 'def' && $mvp_post_layout == '7') ||
					($mvp_post_temp == 'global' && $mvp_post_layout == '1') ||
					($mvp_post_temp == 'global' && $mvp_post_layout == '7') ||
					$mvp_post_temp == 'temp2' ||
					$mvp_post_temp == 'temp8'
				) {
					$mvp_post_side_css = "
        .mvp-post-main-out,
        .mvp-post-main-in {
            margin-right: 0 !important;
            }
        #mvp-post-feat-img img {
            width: 100%;
            }
        #mvp-content-wrap,
        #mvp-post-add-box {
            float: none;
            margin: 0 auto;
            max-width: 750px;
            }
            ";
				}
			}
		}

		$mvp_rtl = get_option('mvp_rtl');
		if ($mvp_rtl == 'true') {
			global $post;
			if (!empty($post)) {
				$mvp_post_layout = get_option('mvp_post_layout');
				$mvp_post_temp = get_post_meta($post->ID, 'mvp_post_template', true);
				if (
					(empty($mvp_post_temp) && $mvp_post_layout == '3') ||
					($mvp_post_temp == 'def' && $mvp_post_layout == '3') ||
					(empty($mvp_post_temp) && $mvp_post_layout == '5') ||
					($mvp_post_temp == 'def' && $mvp_post_layout == '5') ||
					($mvp_post_temp == 'global' && $mvp_post_layout == '3') ||
					($mvp_post_temp == 'global' && $mvp_post_layout == '5') ||
					$mvp_post_temp == 'temp4' ||
					$mvp_post_temp == 'temp6'
				) {
					$mvp_post_side2_css = "
        .mvp-post-main-out,
        .mvp-post-main-in {
            margin-left: 0 !important;
            }
        #mvp-post-feat-img img {
            width: 100%;
            }
        #mvp-post-content,
        #mvp-post-add-box {
            float: none;
            margin: 0 auto;
            max-width: 750px;
            }
            ";
				}
			}
		} else {
			global $post;
			if (!empty($post)) {
				$mvp_post_layout = get_option('mvp_post_layout');
				$mvp_post_temp = get_post_meta($post->ID, 'mvp_post_template', true);
				if (
					(empty($mvp_post_temp) && $mvp_post_layout == '3') ||
					($mvp_post_temp == 'def' && $mvp_post_layout == '3') ||
					(empty($mvp_post_temp) && $mvp_post_layout == '5') ||
					($mvp_post_temp == 'def' && $mvp_post_layout == '5') ||
					($mvp_post_temp == 'global' && $mvp_post_layout == '3') ||
					($mvp_post_temp == 'global' && $mvp_post_layout == '5') ||
					$mvp_post_temp == 'temp4' ||
					$mvp_post_temp == 'temp6'
				) {
					$mvp_post_side2_css = "
        .mvp-post-main-out,
        .mvp-post-main-in {
            margin-right: 0 !important;
            }
        #mvp-post-feat-img img {
            width: 100%;
            }
        #mvp-post-content,
        #mvp-post-add-box {
            float: none;
            margin: 0 auto;
            max-width: 750px;
            }
            ";
				}
			}
		}

		if (is_single()) {
			$mvp_rtl = get_option('mvp_rtl');
			if ($mvp_rtl == 'true') {
				global $post;
				if (!empty($post)) {
					$mvp_post_layout = get_option('mvp_post_layout');
					$mvp_post_temp = get_post_meta($post->ID, 'mvp_post_template', true);
					if (
						(empty($mvp_post_temp) && $mvp_post_layout == '4') ||
						($mvp_post_temp == 'def' && $mvp_post_layout == '4') ||
						(empty($mvp_post_temp) && $mvp_post_layout == '5') ||
						($mvp_post_temp == 'def' && $mvp_post_layout == '5') ||
						($mvp_post_temp == 'global' && $mvp_post_layout == '4') ||
						($mvp_post_temp == 'global' && $mvp_post_layout == '5') ||
						$mvp_post_temp == 'temp5' ||
						$mvp_post_temp == 'temp6'
					) {
						$mvp_post_side3_css = "
        .mvp-nav-soc-wrap {
            margin-top: -15px;
            height: 30px;
            }
        span.mvp-nav-soc-but {
            font-size: 16px;
            padding-top: 7px;
            width: 30px;
            height: 23px;
            }
        #mvp-main-nav-top {
            padding: 10px 0 !important;
            height: 30px !important;
            z-index: 9999;
            }
        .mvp-nav-top-wrap,
        .mvp-nav-top-mid {
            height: 30px !important;
            }
        .mvp-nav-top-mid img {
            height: 100% !important;
            }
        #mvp-main-nav-bot {
            border-bottom: none;
            display: none;
            height: 0;
            }
        .mvp-nav-top-mid img {
            margin-right: 0;
            }
        .mvp-nav-top-left-out {
            margin-right: -200px;
            }
        .mvp-nav-top-left-in {
            margin-right: 200px;
            }
        .mvp-nav-top-left {
            display: block;
            }
            ";
					}
				}
			} else {
				global $post;
				if (!empty($post)) {
					$mvp_post_layout = get_option('mvp_post_layout');
					$mvp_post_temp = get_post_meta($post->ID, 'mvp_post_template', true);
					if (
						(empty($mvp_post_temp) && $mvp_post_layout == '4') ||
						($mvp_post_temp == 'def' && $mvp_post_layout == '4') ||
						(empty($mvp_post_temp) && $mvp_post_layout == '5') ||
						($mvp_post_temp == 'def' && $mvp_post_layout == '5') ||
						($mvp_post_temp == 'global' && $mvp_post_layout == '4') ||
						($mvp_post_temp == 'global' && $mvp_post_layout == '5') ||
						$mvp_post_temp == 'temp5' ||
						$mvp_post_temp == 'temp6'
					) {
						$mvp_post_side3_css = "
        .mvp-nav-soc-wrap {
            margin-top: -15px;
            height: 30px;
            }
        span.mvp-nav-soc-but {
            font-size: 16px;
            padding-top: 7px;
            width: 30px;
            height: 23px;
            }
        #mvp-main-nav-top {
            padding: 10px 0 !important;
            height: 30px !important;
            z-index: 9999;
            }
        .mvp-nav-top-wrap,
        .mvp-nav-top-mid {
            height: 30px !important;
            }
        .mvp-nav-top-mid img {
            height: 100% !important;
            }
        #mvp-main-nav-bot {
            border-bottom: none;
            display: none;
            height: 0;
            }
        .mvp-nav-top-mid img {
            margin-left: 0;
            }
        .mvp-nav-top-left-out {
            margin-left: -200px;
            }
        .mvp-nav-top-left-in {
            margin-left: 200px;
            }
        .mvp-nav-top-left {
            display: block;
            }
            ";
					}
				}
			}
		}

		$mvp_alp = get_option('mvp_alp');
		$mvp_alp_side = get_option('mvp_alp_side');
		if ($mvp_alp == 'true') {
			if (isset($mvp_alp)) {
				if ($mvp_alp_side == '0') {
					$mvp_alp_css = "
        .mvp-auto-post-grid {
            grid-template-columns: 340px minmax(0, auto);
        }
            ";
				} elseif ($mvp_alp_side == '1') {
					$mvp_alp_css = "
        .mvp-alp-side {
            display: none;
        }
        .mvp-alp-soc-reg {
            display: block;
        }
        .mvp-auto-post-grid {
            grid-template-columns: minmax(0, auto) 320px;
            grid-column-gap: 60px;
        }
        @media screen and (max-width: 1199px) {
            .mvp-auto-post-grid {
                grid-column-gap: 30px;
            }
        }
            ";
				} else {
					$mvp_alp_css = "
        .mvp-alp-side {
            display: none;
        }
        .mvp-alp-soc-reg {
            display: block;
        }
        .mvp-auto-post-grid {
            grid-template-columns: 100%;
            margin: 30px auto 0;
            max-width: 1000px;
        }
        .mvp-auto-post-main #mvp-content-body {
            float: none;
            margin: 0 auto;
            max-width: 740px;
        }
            ";
				}
			}
		}

		$mvp_alp_ad = get_option('mvp_alp_ad');
		if (!$mvp_alp_ad) {
			$mvp_alp_side_ad_css = "
        .alp-advert {
            display: none;
        }
        .alp-related-posts-wrapper .alp-related-posts .current {
            margin: 0 0 10px;
        }
            ";
		}

		if ($mvp_customcss) {
			$mvp_customcss_css = "
         $mvp_customcss
            ";
		}

		if (isset($mvp_theme_options)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_theme_options)));
		}
		if (isset($mvp_wall_ad_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', $mvp_wall_ad_css));
		}
		if (isset($mvp_prime_skin_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_prime_skin_css)));
		}
		if (isset($mvp_site_skin_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_site_skin_css)));
		}
		if (isset($mvp_nav_skin_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_nav_skin_css)));
		}
		if (isset($mvp_nav_layout_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_nav_layout_css)));
		}
		if (isset($mvp_para_lead_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_para_lead_css)));
		}
		if (isset($mvp_infinite_scroll_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_infinite_scroll_css)));
		}
		if (isset($mvp_respond_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', $mvp_respond_css));
		}
		if (isset($mvp_cont_read_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_cont_read_css)));
		}
		if (isset($mvp_post_side_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_post_side_css)));
		}
		if (isset($mvp_post_side2_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_post_side2_css)));
		}
		if (isset($mvp_post_side3_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_post_side3_css)));
		}
		if (isset($mvp_alp_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_alp_css)));
		}
		if (isset($mvp_alp_side_ad_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_alp_side_ad_css)));
		}
		if (isset($mvp_customcss_css)) {
			wp_kses_post(wp_add_inline_style('mvp-custom-style', minify($mvp_customcss_css)));
		}
	}
}

add_action('wp_enqueue_scripts', 'mvp_styles_method');


