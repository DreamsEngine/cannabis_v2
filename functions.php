<?php
/*---[ Details ]---------------------------------------
Function 1.0
Author: Adrian Galvez G.
Contact: adrian@dreamsengine.io
-------------------------------------------------------*/

/*-----------------------------------------------------
[00] Global Variables
[01] Base & Framwork Options
[02] Load Javascript
[03] Plugins
[04] Custom Post Types and Taxonomies
[05] Meta Box Panel
-------------------------------------------------------*/

/* [00] Global Variables
-------------------------------------------------------*/
$themename = "maximo";
$prefix = "den_";
$denCDN = "https://cdn.devstate.de";
$denSiteURL = "https://dreamsengine.io";

/* Define Lib */
define('LIB_DIR', trailingslashit(get_stylesheet_directory(). '/lib'));

require_once LIB_DIR.'log.inc.php';

/* Define Includes */
define('INC_DIR', trailingslashit(get_stylesheet_directory(). '/inc'));
/* Define Dreams Plugins*/
define( 'DREAMS_DIR', trailingslashit( get_stylesheet_directory(). '/inc/plugins/dreams' ) );



/* [01] Base & Framwork Options
-------------------------------------------------------*/
require_once INC_DIR .  'op/op-base.php';

/* [03] Dashboard & Framework Options
-------------------------------------------------------*/
require_once INC_DIR . "op/op-dashboard.php";

/* [04] Load JS$
-------------------------------------------------------*/
require_once INC_DIR .  'op/op-loader.php';

/* [05] Load JS$
-------------------------------------------------------*/
require_once INC_DIR .  'extras.php';

